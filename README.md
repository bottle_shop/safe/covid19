# Witbier VII - COVID-19 Online Abusive Behaviors Analysis

Dataset repository for the paper "Covid-19: Anti-Social Behaviors Amid Pandemic ". <br>
A ~40M COVID-19 tweet dataset for hate speech detection :) 

###  Data Collection
We  focus  our  data  collection  efforts  on  the  Twitter  platform.  To  retrievea  set  of  COVID-19  related  tweets,  we  first  define  a  set  of  case-sensitive  key-words that are frequently used in the COVID-19 news and discussions. Thesekeywords include: “covid-19”, “COVID-19”, “COVID”, “Coronavirus”, “coron-avirus”, “CoronaVirus”, and “corona”. Next, we utilize Twitter’s Streaming API to retrieve a set of COVID-19 related tweets using the earlier defined keywordsas queries. The comments on these retrieved tweets are also collected. 

### Annotation Method
<!-- ![Annotation Framework](img/annotation-corona.png) -->
<img src="img/annotation-corona.png" width="800" height="350">


### Result for the automatic annotation of the COVID-19 dataset

| Method | Antisocial Tweets | Normal tweets |
|:---------------|:--------------|:-------------------|
| Lexicon-based | 1,169,755    | 39,215,502 |
| Perspective API | 2,383,316 | 38,001,941 |
| Combined | 2,659,585 | 37,725,672 |


### Statistics on data
- For this study, tweets from 17 March 2020 to 28 April 2020 are collected. 
- We further filterand remove the non-English tweets in our collected dataset, resulting in a total of 40,385,257 tweets retrieved.



### How to use this data?
1. Find annotated dataset in `annotation` dir
2. Find a dumps of tweets dump on `data-garage` dir 
3. We have also developed a rich hate lexicons for COVID-19. You can download it from `hate-lexicons` dir


### To cite 
```
@article{awal2020analyzing,
  title={On Analyzing Antisocial Behaviors Amid COVID-19 Pandemic},
  author={Awal, Md Rabiul and Cao, Rui and Mitrovic, Sandra and Lee, Roy Ka-Wei},
  journal={arXiv preprint arXiv:2007.10712},
  year={2020}
}
```
